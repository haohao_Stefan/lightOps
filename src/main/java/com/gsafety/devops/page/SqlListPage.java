package com.gsafety.devops.page;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gsafety.devops.entity.SqlListEntity;
import com.gsafety.devops.entity.SqlTaskEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;

/**   
 * @Title: Entity
 * @Description: SQL列表
 * @author onlineGenerator
 * @date 2018-09-26 16:12:20
 * @version V1.0   
 *
 */
public class SqlListPage implements java.io.Serializable {
	/**主键*/
	private java.lang.String id;
	/**SQL描述*/
    @Excel(name="SQL描述")
	private java.lang.String sqlName;
	/**SQL类型*/
    @Excel(name="SQL类型")
	private java.lang.String sqlType;
	/**SQL内容*/
    @Excel(name="SQL内容")
	private java.lang.String sqlContent;
	/**创建人名称*/
	private java.lang.String createName;
	/**创建日期*/
	private java.util.Date createDate;
	/**创建人登录名称*/
	private java.lang.String createBy;
	/**更新人名称*/
	private java.lang.String updateName;
	/**更新人登录名称*/
	private java.lang.String updateBy;
	/**更新日期*/
	private java.util.Date updateDate;
	/**所属部门*/
	private java.lang.String sysOrgCode;
	/**所属公司*/
	private java.lang.String sysCompanyCode;
	/**流程状态*/
	private java.lang.String bpmStatus;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  主键
	 */
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  主键
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  SQL描述
	 */
	public java.lang.String getSqlName(){
		return this.sqlName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  SQL描述
	 */
	public void setSqlName(java.lang.String sqlName){
		this.sqlName = sqlName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  SQL类型
	 */
	public java.lang.String getSqlType(){
		return this.sqlType;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  SQL类型
	 */
	public void setSqlType(java.lang.String sqlType){
		this.sqlType = sqlType;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  SQL内容
	 */
	public java.lang.String getSqlContent(){
		return this.sqlContent;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  SQL内容
	 */
	public void setSqlContent(java.lang.String sqlContent){
		this.sqlContent = sqlContent;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建人名称
	 */
	public java.lang.String getCreateName(){
		return this.createName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建人名称
	 */
	public void setCreateName(java.lang.String createName){
		this.createName = createName;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  创建日期
	 */
	public java.util.Date getCreateDate(){
		return this.createDate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  创建日期
	 */
	public void setCreateDate(java.util.Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建人登录名称
	 */
	public java.lang.String getCreateBy(){
		return this.createBy;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建人登录名称
	 */
	public void setCreateBy(java.lang.String createBy){
		this.createBy = createBy;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  更新人名称
	 */
	public java.lang.String getUpdateName(){
		return this.updateName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  更新人名称
	 */
	public void setUpdateName(java.lang.String updateName){
		this.updateName = updateName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  更新人登录名称
	 */
	public java.lang.String getUpdateBy(){
		return this.updateBy;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  更新人登录名称
	 */
	public void setUpdateBy(java.lang.String updateBy){
		this.updateBy = updateBy;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  更新日期
	 */
	public java.util.Date getUpdateDate(){
		return this.updateDate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  更新日期
	 */
	public void setUpdateDate(java.util.Date updateDate){
		this.updateDate = updateDate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  所属部门
	 */
	public java.lang.String getSysOrgCode(){
		return this.sysOrgCode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  所属部门
	 */
	public void setSysOrgCode(java.lang.String sysOrgCode){
		this.sysOrgCode = sysOrgCode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  所属公司
	 */
	public java.lang.String getSysCompanyCode(){
		return this.sysCompanyCode;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  所属公司
	 */
	public void setSysCompanyCode(java.lang.String sysCompanyCode){
		this.sysCompanyCode = sysCompanyCode;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  流程状态
	 */
	public java.lang.String getBpmStatus(){
		return this.bpmStatus;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  流程状态
	 */
	public void setBpmStatus(java.lang.String bpmStatus){
		this.bpmStatus = bpmStatus;
	}

	/**保存-执行任务*/
    @ExcelCollection(name="执行记录")
	private List<SqlTaskEntity> sqlTaskList = new ArrayList<SqlTaskEntity>();
	public List<SqlTaskEntity> getSqlTaskList() {
		return sqlTaskList;
	}
	public void setSqlTaskList(List<SqlTaskEntity> sqlTaskList) {
		this.sqlTaskList = sqlTaskList;
	}
	
	@JsonIgnore
	private List<SqlListEntity> sqlListList = new ArrayList<SqlListEntity>();
	public List<SqlListEntity> getSqlListList() {
		return sqlListList;
	}
	public void setSqlListList(List<SqlListEntity> sqlListList) {
		this.sqlListList = sqlListList;
	}
}
